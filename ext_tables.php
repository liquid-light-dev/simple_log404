<?php
defined('TYPO3_MODE') || die();

(static function() {
    $extensionName = 'SimpleLog404';
    $controllerActions = [
        \Nerdost\SimpleLog404\Controller\LogEntryController::class => 'list',
    ];

    // Old syntax for v9 sites
    if (version_compare(\TYPO3\CMS\Core\Utility\VersionNumberUtility::getNumericTypo3Version(), '10.4.0', '<')) {
        $extensionName = 'Nerdost.SimpleLog404';
        $controllerActions = [
            LogEntry::class => 'list',
        ];
    }

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        $extensionName,
        'site',
        'main',
        '',
        $controllerActions,
        [
            'access' => 'user,group',
            'icon'   => 'EXT:simple_log404/Resources/Public/Icons/Extension.svg',
            'labels' => 'LLL:EXT:simple_log404/Resources/Private/Language/locallang_main.xlf',
            'navigationComponentId' => '',
            'inheritNavigationComponentFromMainModule' => false
        ]
    );

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_simplelog404_domain_model_logentry', 'EXT:simple_log404/Resources/Private/Language/locallang_csh_tx_simplelog404_domain_model_logentry.xlf');
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_simplelog404_domain_model_logentry');
})();
