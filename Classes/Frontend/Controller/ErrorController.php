<?php
namespace Nerdost\SimpleLog404\Frontend\Controller;

use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use Psr\Http\Message\ServerRequestInterface;
use Nerdost\SimpleLog404\Domain\Repository\LogEntryRepository;

class ErrorController extends \TYPO3\CMS\Frontend\Controller\ErrorController
{

    /**
     * @param ServerRequestInterface $request
     * @param string $message
     * @param array $reasons
     * @return ResponseInterface
     * @throws \RuntimeException
     * @throws NoSuchCacheException
     */
    public function pageNotFoundAction(ServerRequestInterface $request, string $message, array $reasons = []): ResponseInterface
    {
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        $objectManager->get(LogEntryRepository::class)->log(404, $request, $message);

        return parent::pageNotFoundAction($request, $message, $reasons);
    }
}
