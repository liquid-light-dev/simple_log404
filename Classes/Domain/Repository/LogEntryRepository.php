<?php

declare(strict_types=1);

namespace Nerdost\SimpleLog404\Domain\Repository;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use Psr\Http\Message\ServerRequestInterface;
use Nerdost\SimpleLog404\Domain\Model\LogEntry;

/**
 * This file is part of the "404 Logging" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 Paul Beck <p.beck@nerdost.net>, Nerdost GmbH
 */

/**
 * The repository for LogEntries
 */
class LogEntryRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    protected $defaultOrderings = [
        'hitcount' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING
    ];

    /**
     * log
     *
     * Log a new 404 or update the hitcount on an existing one
     *
     * @param int $statusCode
     * @param ServerRequestInterface $request
     * @param string $message
     */
    public function log(int $statusCode, ServerRequestInterface $request, string $message)
    {
        $params = $request->getAttributes()['normalizedParams'];
        $requestUri = $params->getRequestUrl();

        $existingLogs = $this->findByRequesturl($requestUri);

        // Do we have an existing log to update?
        if($existingLogs->count()) {
            foreach($existingLogs as $existingLog) {
                // Increment the count by 1
                $count = ((int)$existingLog->getHitcount()) + 1;
                $existingLog->setHitcount((string)$count);
                $this->update($existingLog);
            }
        } else {

            // Make a new Log Entry
            $logEntry = GeneralUtility::makeInstance(LogEntry::class);
            $logEntry->setRequesturl($requestUri);
            $logEntry->setStatuscode($statusCode);
            $logEntry->setMessage($message);
            $logEntry->setLasthit(time());
            $logEntry->setHitcount('1');

            $this->add($logEntry);
        }

        // Save anything created or modified
        $this->persistenceManager->persistAll();
    }

    /**
     * delete
     *
     * Delete an entry by UID
     *
     * @param mixed $uid
     */
    public function deleteByUid(int $uid)
    {
        $entry = $this->findByUid($uid);
        if($entry) {
            $this->remove($entry);
            $this->persistenceManager->persistAll();
        }
    }

}
