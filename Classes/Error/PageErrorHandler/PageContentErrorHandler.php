<?php
namespace Nerdost\SimpleLog404\Error\PageErrorHandler;

use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use Psr\Http\Message\ServerRequestInterface;
use Nerdost\SimpleLog404\Domain\Repository\LogEntryRepository;

class PageContentErrorHandler extends \TYPO3\CMS\Core\Error\PageErrorHandler\PageContentErrorHandler
{

    /**
     * @param ServerRequestInterface $request
     * @param string $message
     * @param array $reasons
     * @return ResponseInterface
     * @throws \RuntimeException
     * @throws NoSuchCacheException
     */
    public function handlePageError(ServerRequestInterface $request, string $message, array $reasons = []): ResponseInterface
    {
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        $objectManager->get(LogEntryRepository::class)->log(404, $request, $message);

        return parent::handlePageError($request, $message, $reasons);
    }

}
