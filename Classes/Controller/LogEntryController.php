<?php

declare(strict_types=1);

namespace Nerdost\SimpleLog404\Controller;

use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Backend\Routing\UriBuilder;
use TYPO3\CMS\Core\Http\RedirectResponse;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use Psr\Http\Message\ServerRequestInterface;
use Nerdost\SimpleLog404\Domain\Repository\LogEntryRepository;


/**
 * This file is part of the "404 Logging" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 Paul Beck <p.beck@nerdost.net>, Nerdost GmbH
 */

/**
 * LogEntryController
 */
class LogEntryController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * logEntryRepository
     *
     * @var \Nerdost\SimpleLog404\Domain\Repository\LogEntryRepository
     */
    protected $logEntryRepository = null;

    /**
     * @param \Nerdost\SimpleLog404\Domain\Repository\LogEntryRepository $logEntryRepository
     */
    public function injectLogEntryRepository(\Nerdost\SimpleLog404\Domain\Repository\LogEntryRepository $logEntryRepository)
    {
        $this->logEntryRepository = $logEntryRepository;
    }

    /**
     * action list
     *
     * @return string|object|null|void
     */
    public function listAction()
    {
        $uriBuilder = GeneralUtility::makeInstance(UriBuilder::class);

        $logEntries = $this->logEntryRepository->findAll()->toArray();

        foreach($logEntries as $entry) {

            $entry->setActionurls([
                'delete' => $uriBuilder->buildUriFromRoute(
                    'SimpleLog404Delete',
                    ['uid' => $entry->getUid()]
                )
            ]);
        }

        $this->view->assign('logEntries', $logEntries);
    }

    /**
     * deleteAction
     *
     * Delete a 404 log
     *
     * @param ServerRequestInterface $request
     *
     * @return ResponseInterface
     */
    public function deleteAction(ServerRequestInterface $request): ResponseInterface
    {
        $params = $request->getQueryParams();

        if(isset($params['uid'])) {
            $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
            $objectManager->get(LogEntryRepository::class)->deleteByUid((int)$params['uid']);
        }

        // Redirect back to module
        $uriBuilder = GeneralUtility::makeInstance(UriBuilder::class);
        return new RedirectResponse($uriBuilder->buildUriFromRoute('site_SimpleLog404Main'));
    }
}
