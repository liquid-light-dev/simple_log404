## How to use

Just install the extension and setup a default PageErrorHandler to display the content of your pages
404 Errorpage.

This Extension will hook inside and log the requested failure URLs which will be displayed ordered
by their hitcount inside a new backend module.

Happy logging!

Thanks to all contributors:

__Mike Street__ <mike@liquidlight.co.uk> who gave this extension some nice improvements and v11 compatibility
