<?php

return [
    // Login screen of the TYPO3 Backend
    'SimpleLog404Delete' => [
        'path' => '/simple_log404/delete',
        'referrer' => 'required,refresh-empty',
        'target' => \Nerdost\SimpleLog404\Controller\LogEntryController::class . '::deleteAction'
    ],
];
